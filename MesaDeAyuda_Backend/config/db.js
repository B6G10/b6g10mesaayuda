//Importar modulo de Mongo DB => mongoose
const  mongoose = require('mongoose');

//importar dependencias para usar varibles de entorno
require('dotenv').config({path:'var.env'});
//const user = 'b06g10';
//const psw = 'b06g10app';
//const db = 'mesadeayuda';
//const url = `mongodb+srv://${user}:${psw}@cluster0.ppbuj.mongodb.net/${db}?retryWrites=true&w=majority`;
//mongodb+srv://b06g10:b06g10app@cluster0.ppbuj.mongodb.net/mesadeayuda?retryWrites=true&w=majority
const conexionDB3 = async () => {
    try {
        await mongoose.connect('mongodb+srv://b06g10:b06g10app@cluster0.ppbuj.mongodb.net/mesadeayuda?retryWrites=true&w=majority');
        console.log('Conexión establecida con MongoDB Atlas');
    } catch (error) {
        console.log('Error de conexión a la Base de datos. (config/db.js)');
        console.log(error);
        process.exit(1);
    }
}

const conexionDB = async () => {
    try {
        await mongoose.connect(process.env.URL_MONGODB);
        console.log('Conexión establecida con MongoDB Atlas');
    } catch (error) {
        console.log('Error de conexión a la Base de datos.');
        console.log(error);
        process.exit(1);
    }
}

module.exports = conexionDB;