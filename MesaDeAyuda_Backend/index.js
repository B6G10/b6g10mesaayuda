//Importar express - sintaxis ES6 => Estandar de JavaScritp
//import express from 'express';
//Sintaxis nativa de nodejs
const express = require('express');

const bodyParser = require('body-parser');
//const { Mongoose } = require('mongoose');

const router = express.Router();

//Importar modulo de Mongo DB => mongoose
//const  mongoose = require('mongoose');

//importar dependencias para usar varibles de entorno
//require('dotenv').config({path:'var.env'});
const conectarDB = require('./config/db');

let app = express();
app.use(express.json());
//app.use('/',function(req,res){
//    res.send('Hola Tripulantes');
//});

app.use(router);

//conexion BD
conectarDB();

// Integración del Frontend en el Backend
app.use(express.static('public'));



//modelo o esquema (schema) de la base de datos
/*const usuarioSchema = new mongoose.Schema({
    //id : {type: Number, require : true, default : 0},
    id : Number,
    nombre : String,
    email : String,
    constrasegna : String
});*/

//modelo del usuario=> tener encuenta el esquema para la conexion
//const modeloUsuario = mongoose.model('modelo_usuario',usuarioSchema);

/*
modeloUsuario.create(
    {
        id : 4,
        nombre : "Nombre usuario 4",
        email : "usuario4@usuario.com",
        constrasegna : "12345"
    
    },
    (error) => {
        //console.log("Ha ocurrido el siguiente error");
        if(error) return console.log(error);
    }
);
*/
// CRUD => Read 
// modeloUsuario.find((error, usuarios) => { if (error) return console.log(error); console.log(usuarios); }) 
// CRUD => Update 
/*modeloUsuario.updateOne({id: 4}, {nombre: "Usuario nombre 4 Act"}, 
    (error) => { 
         if (error) return console.log(error);  
    }
);*/

// // CRUD => Delete 
// modeloUsuario.deleteOne({id: 4}, (error) => { // if (error) return console.log(error); // })


// Rutas repecto al CRUD
// // uso de archivos tipo json en la app

// CORS (Cross-Origin Resource Sharing) => mecanismo o reglas de seguridad para el control de peticiones http
const cors = require('cors');
app.use(cors());

// Solución temporal al erros CORS
var whitelist = ['http://localhost:4000/', 'http://localhost:4200/']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { Origin: true } // reflect (enable) the requested Origin in the CORS response
  }else{
    corsOptions = { Origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}


const crudUsuarios = require('./controllers/controlusuarios');
const crudTickets = require('./controllers/controltickets');


router.post('/api/auth',cors(corsOptionsDelegate),crudUsuarios.login);
router.post('/',cors(corsOptionsDelegate),crudUsuarios.crear);
router.get('/',cors(corsOptionsDelegate),crudUsuarios.obtener);
router.get('/:id',cors(corsOptionsDelegate),crudUsuarios.obtenerPorId);
router.put('/:id',cors(corsOptionsDelegate),crudUsuarios.actualizar);
router.delete('/:id',cors(corsOptionsDelegate),crudUsuarios.eliminar);

router.post('/api/ticket',cors(corsOptionsDelegate),crudTickets.crear);
router.get('/api/ticket',cors(corsOptionsDelegate),crudTickets.obtener);
router.get('/api/ticket/:id',cors(corsOptionsDelegate),crudTickets.obtenerPorId);
router.put('/api/ticket/:id',cors(corsOptionsDelegate),crudTickets.actualizar);
router.delete('/api/ticket/:id',cors(corsOptionsDelegate),crudTickets.eliminar);

/*
router.post('/',crudUsuarios.crear);
router.get('/',crudUsuarios.obtener);
router.put('/:id',crudUsuarios.actualizar);
router.delete('/:id',crudUsuarios.eliminar);
*/

/*
router.get('/',function(req, res){
    res.send("Uso de método GET");
}); */

/*
router.post('/metodopost',function(req,res){
    res.send("Uso de método POST");
});*/






//Conexion con la base de datos
/*
const user = 'b06g10';
const psw = 'b06g10app';
const db = 'mesadeayuda';
const url = `mongodb+srv://${user}:${psw}@cluster0.ppbuj.mongodb.net/${db}?retryWrites=true&w=majority`;

mongoose.connect(url)
    .then(function (){console.log("Conexion Establecida con MongoDB Atlas")
                        console.log(url)})
    .catch(function(e){console.log(e)})
    */

app.listen(4000);

console.log("La aplicacion se ejecuta en:http://localhost:4000");
