//Importar modelo de MongoDB => mongoose
const mongoose = require ('mongoose');

//modelo o esquema (schema) de la base de datos
const estadoSchema = new mongoose.Schema({
    //id : {type: Number, require : true, default : 0},
    idtickect : Number,
    titulo : String,
    descripcion : String,
    usuarioCrea : Number,
    
},{
    versionKey : false,
    timestamps : true,
});

//modelo del usuario=> tener encuenta el esquema para la conexion
module.exports = mongoose.model('estados', estadoSchema);