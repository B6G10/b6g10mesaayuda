//Importar modelo de MongoDB => mongoose
const mongoose = require ('mongoose');

//modelo o esquema (schema) de la base de datos
const usuarioSchema = new mongoose.Schema({
    //id : {type: Number, require : true, default : 0},
    cui : String,
    nombre : String,
    email : String,
    password : String,
    perfil : String
},{
    versionKey : false,
    timestamps : true,
});

//modelo del usuario=> tener encuenta el esquema para la conexion
module.exports = mongoose.model('usuarios', usuarioSchema);