//Importar modelo de MongoDB => mongoose
const mongoose = require ('mongoose');
const modeloUsuario = require('../models/modelsusuarios');

//modelo o esquema (schema) de la base de datos
const ticketSchema = new mongoose.Schema({
    titulo : String,
    descripcion : String,
    fechaCierre : { type: Date },
    estado : String,
    usuarioCrea : String,
    usuarioAsignado : String,

},{
    versionKey : false,
    timestamps : true,
});

//modelo del ticket=> tener encuenta el esquema para la conexion
module.exports = mongoose.model('tickets', ticketSchema);