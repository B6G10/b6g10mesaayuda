//Importar modelo DB
const modeloUsuario = require('../models/modelsusuarios');

//Exportar en diferentes variables los metodos para rl CRUD

//login
exports.login = async (req,res) => {
    try {
        //const usuario = await modeloUsuario.find({cui:{$eq:valor}});
        const usuario = await modeloUsuario.find({email:{$eq:req.body.email}});
        if(!usuario){
            console.log(usuario);
            res.status(404).json({ msg:"El usuario no existe"  });
        }else{
            res.send(usuario);
        }
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al obtener usuario(s)");
    }
}


//CRUD - Create
exports.crear = async (req, res) => {

    try {

        let usuario;
        console.log(req);
        console.log("req.body:", req.body);
        usuario = new modeloUsuario(req.body);
        /*usuario = new modeloUsuario( {
            cui : "1090123456_2",
            nombre : "Usuario Admmin 2",
            email : "admin2@admin.com",
            password : 12345,
            perfil : ""
        });*/
        await usuario.save();

        res.send(usuario);

    } catch (error) {
        console.log(error);
        res.status(500).send("Error al guaradar usuario");
    }


} 

//CRUD - Read
exports.obtener = async (req,res)=>{
    try {
        const usuario = await modeloUsuario.find();
        res.json(usuario);
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al obtener usuario(s)");
    }
};

exports.obtenerPorId = async (req,res)=>{
    try {
        const usuario = await modeloUsuario.findById(req.params.id);
        if(!usuario){
            console.log(usuario);
            res.status(404).json({msg:"El usuario no existe"});
        }else{
            res.json(usuario);
        }
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al obtener usuario(s)");
    }
};

//CRUD - Update
exports.actualizar = async (req,res)=>{
    try {
        const usuario = await modeloUsuario.findById(req.params.id);
        if(!usuario){
            console.log(usuario);
            res.status(404).json({msg:"El usuario no existe"});
        }else{
            console.log(req.body);
            await modeloUsuario.findByIdAndUpdate({_id : req.params.id},req.body);         
            res.json({msg: "Usuario actualizado"});
        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al actualizar usuario");
    }
};

exports.actualizarCUI = async (req,res)=>{
    try {
        console.log(typeof req.params.id);
        let valor = Number(req.params.id);
        console.log(typeof valor);
        const usuario = await modeloUsuario.find({cui:{$eq:valor}});
        if(!usuario){
            console.log(usuario);
            res.status(404).json({msg:"El usuario no existe"});
        }else{
            await modeloUsuario.findOneAndUpdate({cui:{$eq:valor}},{nombre : "Usuario 4 Actualizado"});          
            res.json({msg: "Usuario actualizado"});
        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al actualizar usuario");
    }
};


//CRUD - Delete
exports.eliminar = async (req,res)=>{
    try {
        const usuario = await modeloUsuario.findById(req.params.id);
        console.log("borrar backend");
        if(!usuario){
            console.log(usuario);
            res.status(404).json("El usuario no existe");
        }else{
            await modeloUsuario.findByIdAndRemove({_id: req.params.id});          
            res.json({msg: "Usuario Borrado"});
        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al borrar usuario");
    }
};