//Importar modelo DB
const modeloEstado = require('../models/modelestados');

//Exportar en diferentes variables los metodos para rl CRUD

//CRUD - Create
exports.crear = async (req,res) => {

    try{

        let estado;
        estado = new modeloEstado( {
            idticket : "idTicket",
            titulo : "estado Titulo",
            descripcion : "estado descrpcion",
            usuarioCrea : "Estado usuario crea"
        });

        await estado.save();

        res.send(estado);

    }catch(error){
        console.log(error);
        res.status(500).send("Error al guaradar estado");
    }
  

} 

//CRUD - Read
exports.obtener = async (req,res)=>{
    try {
        const estado = await modeloEstado.find();
        res.json(estado);
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al obtener estado(s)");
    }
};

//CRUD - Update
exports.actualizar = async (req,res)=>{
    try {
        const estado = await modeloEstado.findById(req.params.id);
        if(!estado){
            console.log(estado);
            res.status(404).json({msg:"El estado no existe"});
        }else{
            await modeloEstado.findByIdAndUpdate({_id : req.params.id},{nombre : "estado Admin Actualizado"});         
            res.json({msg: "estado actualizado"});
        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al actualizar estado");
    }
};

exports.actualizarCUI = async (req,res)=>{
    try {
        console.log(typeof req.params.id);
        let valor = Number(req.params.id);
        console.log(typeof valor);
        const estado = await modeloEstado.find({cui:{$eq:valor}});
        if(!estado){
            console.log(estado);
            res.status(404).json({msg:"El estado no existe"});
        }else{
            await modeloEstado.findOneAndUpdate({cui:{$eq:valor}},{nombre : "estado 4 Actualizado"});          
            res.json({msg: "estado actualizado"});
        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al actualizar estado");
    }
};


//CRUD - Delete
exports.eliminar = async (req,res)=>{
    try {
        const estado = await modeloEstado.findById(req.params.id);
        if(!estado){
            console.log(estado);
            res.status(404).json("El estado no existe");
        }else{
            await modeloEstado.findByIdAndRemove({_id: req.params.id});          
            res.json({msg: "estado Borrado"});
        }
        res.json(estado);
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al borrar estado");
    }
};