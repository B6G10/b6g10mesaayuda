//Importar modelo DB
const modeloTicket = require('../models/modeltickets');

//Exportar en diferentes variables los metodos para rl CRUD

//CRUD - Create
exports.crear = async (req,res) => {

    try{

        let ticket;
        console.log("ModeloTicket _ Crear")
        console.log(req.body);
        ticket = new modeloTicket(req.body);

        await ticket.save();

        res.send(ticket);

    }catch(error){
        console.log(error);
        res.status(500).send("Error al guaradar ticket");
    }
  

} 

//CRUD - Read
exports.obtener = async (req,res)=>{
    try {
        const ticket = await modeloTicket.find();
        res.json(ticket);
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al obtener tickets(s)");
    }
};

exports.obtenerPorId = async (req,res)=>{
    try {
        const ticket = await modeloTicket.findById(req.params.id);
        if(!ticket){
            console.log(ticket);
            res.status(404).json({msg:"El ticket no existe"});
        }else{
            res.json(ticket);
        }
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al obtener ticket(s)");
    }
};

//CRUD - Update
exports.actualizar = async (req,res)=>{
    try {
        const ticket = await modeloTicket.findById(req.params.id);
        if(!ticket){
            console.log(ticket);
            res.status(404).json({msg:"El ticket no existe"});
        }else{
            await modeloTicket.findByIdAndUpdate({_id : req.params.id},req.body);         
            res.json({msg: "Ticket actualizado"});
        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al actualizar ticket");
    }
};



//CRUD - Delete
exports.eliminar = async (req,res)=>{
    try {
        const ticket = await modeloTicket.findById(req.params.id);
        if(!ticket){
            console.log(ticket);
            res.status(404).json("El ticket no existe");
        }else{
            await modeloTicket.findByIdAndRemove({_id: req.params.id});          
            res.json({msg: "ticket Borrado"});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send("Error al borrar ticket");
    }
};