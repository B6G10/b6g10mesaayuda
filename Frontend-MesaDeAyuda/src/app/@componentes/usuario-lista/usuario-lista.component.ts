import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RespuestaApi } from 'src/app/@modelos/respuesta-api';
import { Usuario } from 'src/app/@modelos/usuario.model';
import { SharedService } from 'src/app/@servicios/shared.service';
import { UsuarioService } from 'src/app/@servicios/usuario.service';
import { DialogoService } from 'src/app/dialogo.service';
import { audit } from 'rxjs';

@Component({
  selector: 'app-usuario-lista',
  templateUrl: './usuario-lista.component.html',
  styleUrls: ['./usuario-lista.component.css']
})
export class UsuarioListaComponent implements OnInit {

  page:number=0;
  count:number=5;
  pages!: Array<number>;
  message:{} | any;
  classCss: {} | any;

  shared: SharedService;
  listUser=[] as any;
  usuarios : any [] = [
    {
      id : "1",
      email : "admin@admin",
      password : "123",
      profile : "ADMIN"
    },
    {
      id : "2",
      email : "tech@admin",
      password : "123",
      profile : "TECH"
    },
    {
      id : "3",
      email : "user@admin",
      password : "123",
      profile : "user"
    }
  ]

  constructor(private dialogoService: DialogoService,
              private usuarioService: UsuarioService,
              private router: Router) {
                this.shared = SharedService.getInstance();
               }

  ngOnInit(): void {
    this.findAll();
  }

  findAll(){
    this.usuarioService.findAll()
      .subscribe(datos => {
        this.listUser = datos;
        console.log(this.listUser);
      })
  }

  edit(id:string){
    console.log("Id Editar");
    console.log(id);
    this.router.navigate(['/usuario-nuevo',id]);
  }

  delete(id:string){
    this.dialogoService.confirm(' Esta seguro de borrar usuario ?').then((candelete)=>{
      candelete = <boolean> candelete;
      if(candelete){
        this.usuarioService.delete(id)
          .subscribe(()=>{
              console.log("Usuario borrado");
              this.findAll();
              this.showMessage({
                type: 'success',
                text: 'Record deleted'
              });              
              this.router.navigate(['/usuario-lista']);
          }, err =>{
            this.showMessage({
              type: 'error',
              text: err['error']['errors'][0]
            });
          });
      }
    });

    /*this.dialogoService.confirm(' Do you want to delete the user ?').then((candelete)=>{
      candelete = <boolean> candelete;
      if(candelete){
        this.message = {};
        this.usuarioService.delete(id).subscribe((respuestaApi : any) => {
          respuestaApi = <RespuestaApi> respuestaApi;
          this.showMessage({
            type: 'success',
            text: 'Record deleted'
          });
          //this.findAll(this.page,this.count);
        }, err =>{
          this.showMessage({
            type: 'error',
            text: err['error']['errors'][0]
          });
        });
      }
    });*/
  }

  setNextPage(event:any){
    event.preventDefault();
    if(this.page+1 < this.pages.length){
      this.page = this.page + 1;
      //this.findAll(this.page,this.count);
    }
  }

  setPreviousPage(event:any){
    event.preventDefault();
    if(this.page > 0){
      this.page = this.page - 1;
      //this.findAll(this.page,this.count);
    }
  }

  setPage(i: number,event:any){
    event.preventDefault();
    
    this.page = i;
    //this.findAll(this.page,this.count);
    
  }

  private showMessage(message: {type: string, text: string}) : void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    },3000);
  }

  private buildClasses(type: string) : void{
    this.classCss = {
      'alert' : true
    }
    this.classCss['alert-'+type] = true;
  }
}
