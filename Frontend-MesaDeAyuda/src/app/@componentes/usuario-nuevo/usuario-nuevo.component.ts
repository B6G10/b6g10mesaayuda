import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/@modelos/usuario.model';
import { SharedService } from 'src/app/@servicios/shared.service';
import { UsuarioService } from 'src/app/@servicios/usuario.service';
import { UsuarioBD } from '../../@modelos/usuario-bd';

@Component({
  selector: 'app-usuario-nuevo',
  templateUrl: './usuario-nuevo.component.html',
  styleUrls: ['./usuario-nuevo.component.css']
})
export class UsuarioNuevoComponent implements OnInit {


  @ViewChild("form")
  form!: NgForm;

  usuario = new Usuario('','','','','','');
  shared : SharedService;
  message : {} | any;
  classCss : {} | any;

  constructor(private usuarioService: UsuarioService,
    private route: ActivatedRoute) { 
      this.shared = SharedService.getInstance();
    }

  ngOnInit(): void {
    let id: string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findById(id);
    }
  }

  findById(id: string){
    this.usuarioService.findById(id)
      .subscribe(datos => {
        this.usuario = <any>datos;
        console.log("Despues de encontrar el usuario");
        console.log(this.usuario);
    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    }
    );
    /*this.userService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.user = responseApi.data;
      this.user.password = '';
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });*/
  }

  register(){
    this.message = {};
    console.log("Datos a guardar");
    console.log(this.usuario)
    this.usuarioService.createOrUpdate(this.usuario)
      .subscribe(datos => {
        this.usuario = new Usuario('','','','','','');
        let usuarioresp : any = datos;
        console.log("Despues de actualizar el usuario");
        console.log(usuarioresp);
        this.showMessage({
          type: 'success',
          text:`Registrado ${usuarioresp.email} satisfactoriamente`
        });
      },err => {
        this.showMessage({
          type: 'error',
          text: err['error']['errors'][0]
        });
      }      
      );
    //Usuariodb Usuariodb= new Usuariodb(this.usuario.cui,this.usuario.nombre,this.usuario.email,this.usuario.password,this.usuario.profile);
    /*this.userService.createOrUpdate(this.user).subscribe((responseApi: ResponseApi) => {
      this.user = new User('','','','');
      let userRet : User = responseApi.data;
      this.form.resetForm();
      this.showMessage({
        type: 'success',
        text:`Registered ${userRet.email} sucessfully`
      });
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });*/
  }

  private showMessage(message: {type: string, text: string}) : void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    },3000);
  }

  private buildClasses(type: string) : void{
    this.classCss = {
      'alert' : true
    }
    this.classCss['alert-'+type] = true;
  }

  getFromGroupClass(isInvalid: boolean | null, isDirty: boolean | null): {}{
    return{
      'form-group': true,
      'has-error': isInvalid && isDirty,
      'has-success': !isInvalid && isDirty
    };
  }
}
