import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentUser } from 'src/app/@modelos/usuario-actual.model';
import { Usuario } from 'src/app/@modelos/usuario.model';
import { SharedService } from 'src/app/@servicios/shared.service';
import { UsuarioService } from 'src/app/@servicios/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario = new Usuario('','','','','','');
  shared: SharedService;
  message: string | undefined;

  constructor(private usuarioService: UsuarioService, private router: Router) { this.shared = SharedService.getInstance() }

  ngOnInit(): void {
  }

  login(){
    if(this.usuario.email!=''){
      /*this.shared.token = "11111111111";
      this.shared.usuario = new Usuario('1','Admin','1234','aaa','bbb','');
      this.shared.usuario.perfil = "ADMIN";
      this.message = '';
      this.shared.showTemplate.emit(true);
      this.router.navigate(['/']); // Agregado*/

    this.usuarioService.login(this.usuario).subscribe(obj => {
        console.log("Login");
        console.log("Login");
        if(Object.values(obj).length==1){
          let tok = Object.values(obj)[0]._id;
          this.shared.token = Object.values(obj)[0]._id;//datos.token;
          this.shared.usuario = (Object.values(obj)[0]); //datos.usuario;
          this.shared.usuario.perfil = this.shared.usuario.perfil.substring(5);
          this.shared.showTemplate.emit(true);
          this.router.navigate(['/']);
        }else{
          this.shared.token = null;
        this.shared.usuario = null;
        this.shared.showTemplate.emit(false);
        this.message = 'Erro';  
        }
      }, err => {
        this.shared.token = null;
        this.shared.usuario = null;
        this.shared.showTemplate.emit(false);
        this.message = 'Erro';
      });
      }
    }
  
    cancelLogin(){
      this.message = '';      
      this.usuario = new Usuario('','','','','','');
      window.location.href = '/login';
      window.location.reload();
    }
  
    getFromGroupClass(isInvalid: boolean | null, isDirty: boolean | null): {}{
      return{
        'form-group': true,
        'has-error': isInvalid && isDirty,
        'has-success': !isInvalid && isDirty
      };
    }

}
