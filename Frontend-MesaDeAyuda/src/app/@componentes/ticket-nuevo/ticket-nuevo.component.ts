import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Ticket } from 'src/app/@modelos/ticket';
import { SharedService } from 'src/app/@servicios/shared.service';
import { TicketService } from 'src/app/@servicios/ticket.service';

@Component({
  selector: 'app-ticket-nuevo',
  templateUrl: './ticket-nuevo.component.html',
  styleUrls: ['./ticket-nuevo.component.css']
})
export class TicketNuevoComponent implements OnInit {


@ViewChild("form")
form!: NgForm

ticket = new Ticket(null,'','','',null,null,null,null);
shared: SharedService;
message:{} | any;
classCss:{}| any;

  constructor(private ticketService: TicketService,
    private route: ActivatedRoute) { 
      this.shared = SharedService.getInstance();
    }

  ngOnInit(): void {
    let id: string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findById(id);
    }
  }

  findById(id: string){
    this.ticketService.findById(id).subscribe((data) => {
      this.ticket = <any>data;
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  register(){
    this.message = {};
    this.ticket.usuarioCrea = this.shared.usuario.email;
    this.ticketService.createOrUpdate(this.ticket).subscribe(data => {
      this.ticket = new Ticket(null,'','','',null,null,null,null);
      let ticket : any = data;
      this.form.resetForm();
      this.showMessage({
        type: 'success',
        text:`Registered ${ticket.titulo} sucessfully`
      });
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  private showMessage(message: {type: string, text: string}) : void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    },3000);
  }

  private buildClasses(type: string) : void{
    this.classCss = {
      'alert' : true
    }
    this.classCss['alert-'+type] = true;
  }

  getFromGroupClass(isInvalid: boolean | null, isDirty: boolean| null): {}{
    return{
      'form-group': true,
      'has-error': isInvalid && isDirty,
      'has-success': !isInvalid && isDirty
    };
  }

}
