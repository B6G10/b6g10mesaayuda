import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ticket } from 'src/app/@modelos/ticket';
import { SharedService } from 'src/app/@servicios/shared.service';
import { TicketService } from 'src/app/@servicios/ticket.service';
import { DialogoService } from 'src/app/dialogo.service';

@Component({
  selector: 'app-ticket-lista',
  templateUrl: './ticket-lista.component.html',
  styleUrls: ['./ticket-lista.component.css']
})
export class TicketListaComponent implements OnInit {

  assignedToMe: boolean = false;
  page:number = 0;
  count:number = 5;
  pages!: Array<number>;
  shared: SharedService;
  message:{} | any;
  classCss:{} | any;
  listTicket = [] as any;
  ticketFilter = new Ticket(null,'','','',null,null,null,null);

  constructor(private dialogoService : DialogoService,
    private ticketService : TicketService,
    private router: Router) { 
      this.shared = SharedService.getInstance();
    }

  ngOnInit(): void {
    this.findAll();
  }

  findAll(){
    this.ticketService.findAll().subscribe((datos) =>{
      this.listTicket = datos;
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  filter(): void{
    this.page = 0;
    this.count = 5;
    this.ticketService.findByParams(this.ticketFilter)
      .subscribe((datos) =>{
        this.ticketFilter.titulo = this.ticketFilter.titulo == 'NoInformado' ? '' : this.ticketFilter.titulo;
        this.ticketFilter._id = this.ticketFilter._id == '' ? null : this.ticketFilter._id;
        this.listTicket = datos;
      }, err =>{
        this.showMessage({
          type: 'error',
          text: err['error']['errors'][0]
      });
    });
  }

  cleanFilter():void{
    this.assignedToMe = false;
    this.page = 0;
    this.count = 5;
    this.ticketFilter = new Ticket(null,'','','',null,null,null,null);
    this.findAll();
  }

  edit(id:string){
    this.router.navigate(['/ticket-nuevo',id]);
  }

  detail(id:string){
    this.router.navigate(['/ticket-detalle',id]);
  }

  delete(id:string){
    this.dialogoService.confirm(' Confirma borrado de ticket ?').then((candelete)=>{
      if(candelete){
        this.message = {};
        this.ticketService.delete(id).subscribe((datos) => {
          this.findAll();
          this.showMessage({
            type: 'success',
            text: 'Record deleted'
          });
          
          this.router.navigate(['/ticket-lista']);
        }, err =>{
          this.showMessage({
            type: 'error',
            text: err['error']['errors'][0]
          });
        });
      }
    });
  }

  setNextPage(event:any){
    event.preventDefault();
    if(this.page+1 < this.pages.length){
      this.page = this.page + 1;
      this.findAll();
    }
  }

  setPreviousPage(event:any){
    event.preventDefault();
    if(this.page > 0){
      this.page = this.page - 1;
      this.findAll();
    }
  }


  private showMessage(message: {type: string, text: string}) : void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    },3000);
  }

  private buildClasses(type: string) : void{
    this.classCss = {
      'alert' : true
    }
    this.classCss['alert-'+type] = true
  }

}
