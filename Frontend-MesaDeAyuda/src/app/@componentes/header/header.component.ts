import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/@modelos/usuario.model';
import { SharedService } from 'src/app/@servicios/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public shared: SharedService;

  constructor() { 
    this.shared = SharedService.getInstance();
    this.shared.usuario = new Usuario('','','','','','');
  }

  ngOnInit(): void {
  }

  signOut(){
    this.shared.token = '-1';
    this.shared.usuario = new Usuario('-1','','','','','');;
    window.location.href = '/login';
    window.location.reload();
  }

}
