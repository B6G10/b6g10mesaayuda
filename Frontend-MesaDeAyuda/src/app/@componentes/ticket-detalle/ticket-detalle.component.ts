import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TicketBD } from 'src/app/@modelos/ticket-bd';
import { SharedService } from 'src/app/@servicios/shared.service';
import { TicketService } from 'src/app/@servicios/ticket.service';

@Component({
  selector: 'app-ticket-detalle',
  templateUrl: './ticket-detalle.component.html',
  styleUrls: ['./ticket-detalle.component.css']
})
export class TicketDetalleComponent implements OnInit {

  ticket = new TicketBD(null,'','','',null,null,null,null,null,null);
  shared: SharedService;
  message:{} | any;
  classCss:{} | any;

  constructor(private ticketService: TicketService,
    private route: ActivatedRoute) {
      this.shared = SharedService.getInstance();
     }

  ngOnInit(): void {
    let id: string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findById(id);
    }
  }

  findById(id: string){
    this.ticketService.findById(id).subscribe((datos) => {
      this.ticket = <any>datos;
      //this.ticket. = new Date(this.ticket.date).toISOString();
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  changeStatus(status:string):void{
    this.ticketService.changeStatus(status,this.ticket).subscribe((datos) => {
      this.ticket = <any>datos;
      //this.ticket.date = new Date(this.ticket.date).toISOString();
      this.showMessage({
        type: 'success',
        text: 'Successfully changed status'
      });
    }, err =>{
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  private showMessage(message: {type: string, text: string}) : void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    },3000);
  }

  private buildClasses(type: string) : void{
    this.classCss = {
      'alert' : true
    }
    this.classCss['alert-'+type] = true;
  }


}
