import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './@componentes/home/home.component';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from "./@componentes/seguridad/login/login.component";
import { AuthGuard } from "./@componentes/seguridad/auth.guard";
import { UsuarioListaComponent } from './@componentes/usuario-lista/usuario-lista.component';
import { UsuarioNuevoComponent } from './@componentes/usuario-nuevo/usuario-nuevo.component';
import { TicketListaComponent } from "./@componentes/ticket-lista/ticket-lista.component";
import { TicketNuevoComponent } from './@componentes/ticket-nuevo/ticket-nuevo.component';
import { TicketDetalleComponent } from "./@componentes/ticket-detalle/ticket-detalle.component";
import { ResumenComponent } from './@componentes/resumen/resumen.component';

export const ROUTES: Routes = [
    {path : '', component : HomeComponent,canActivate: [AuthGuard]},
    {path : 'login', component : LoginComponent},
    {path: 'usuario-nuevo', component : UsuarioNuevoComponent, canActivate: [AuthGuard]},
    {path: 'usuario-nuevo/:id', component : UsuarioNuevoComponent, canActivate: [AuthGuard]},
    {path: 'usuario-lista', component : UsuarioListaComponent, canActivate: [AuthGuard]},
    {path: 'ticket-lista', component : TicketListaComponent, canActivate: [AuthGuard]},
    {path: 'ticket-nuevo', component : TicketNuevoComponent, canActivate: [AuthGuard]},
    {path: 'ticket-nuevo/:id', component : TicketNuevoComponent, canActivate: [AuthGuard]},
    {path: 'ticket-detalle/:id', component : TicketDetalleComponent, canActivate: [AuthGuard]},
    {path: 'resumen', component : ResumenComponent, canActivate: [AuthGuard]}
]

export const routes: ModuleWithProviders<RouterModule> = RouterModule.forRoot(ROUTES);