import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './@componentes/home/home.component';
import { MenuComponent } from './@componentes/menu/menu.component';
import { HeaderComponent } from './@componentes/header/header.component';
import { FooterComponent } from './@componentes/footer/footer.component';
import { LoginComponent } from './@componentes/seguridad/login/login.component';
import { UsuarioService } from './@servicios/usuario.service';
import { SharedService } from './@servicios/shared.service';
import { routes } from './app.routes';
import { AuthGuard } from './@componentes/seguridad/auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DialogoService } from './dialogo.service';
import { UsuarioListaComponent } from './@componentes/usuario-lista/usuario-lista.component';
import { UsuarioNuevoComponent } from './@componentes/usuario-nuevo/usuario-nuevo.component';
import { TicketListaComponent } from './@componentes/ticket-lista/ticket-lista.component';
import { TicketNuevoComponent } from './@componentes/ticket-nuevo/ticket-nuevo.component';
import { TicketService } from './@servicios/ticket.service';
import { TicketDetalleComponent } from './@componentes/ticket-detalle/ticket-detalle.component';
import { ResumenComponent } from './@componentes/resumen/resumen.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    UsuarioListaComponent,
    UsuarioNuevoComponent,
    TicketListaComponent,
    TicketNuevoComponent,
    TicketDetalleComponent,
    ResumenComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    routes
  ],
  providers: [
    TicketService,
    UsuarioService, 
    SharedService,
    DialogoService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
