import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//Agregar rutas hijas
const routes: Routes = [
  /*{
    path : 'home',
    loadChildren : () => import('./@public/pages/home/home.module').then(m => m.HomeModule)
  }*/
    
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
