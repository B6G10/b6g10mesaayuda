import { MESA_AYUDA_API } from './mesadeayuda.api';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ticket } from '../@modelos/ticket';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private http: HttpClient) { }

  createOrUpdate(ticket: Ticket){
    if(ticket._id != null && ticket._id != ''){
      console.log("Ticket Service _ Actualizar");
      console.log(ticket)
      return this.http.put(`${MESA_AYUDA_API}/api/ticket/${ticket._id }`, ticket);
    }else{
      ticket._id = null;
      ticket.estado = 'Nuevo';
      return this.http.post(`${MESA_AYUDA_API}/api/ticket`, ticket);
    }
  }

  findAll(){
    return this.http.get(`${MESA_AYUDA_API}/api/ticket`);
  }

  findById(id: string){
    return this.http.get(`${MESA_AYUDA_API}/api/ticket/${id}`);
  }

  delete(id: string){
    return this.http.delete(`${MESA_AYUDA_API}/api/ticket/${id}`);
  }

  findByParams(t:Ticket){
    t._id = t._id == null ? '' : t._id;
    t.titulo = t.titulo == '' ? 'NoInformado' : t.titulo;
    t.estado = t.estado == '' ? 'NoInformado' : t.estado;
    return this.http.get(`${MESA_AYUDA_API}/api/ticket/${t._id}/${t.titulo}/${t.estado}`);
  }

  changeStatus(estado: string, ticket: Ticket){
    return this.http.put(`${MESA_AYUDA_API}/api/ticket/${ticket._id}/${estado}`, ticket);
  }

  summary(){
    return this.http.get(`${MESA_AYUDA_API}/api/ticket/summary`);
  }
}
