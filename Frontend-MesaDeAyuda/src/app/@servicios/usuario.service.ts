import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MESA_AYUDA_API } from './mesadeayuda.api';
import { Usuario } from '../@modelos/usuario.model';
import { UsuarioBD } from '../@modelos/usuario-bd';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  login(usuario: Usuario){
    return this.http.post(`${MESA_AYUDA_API}/api/auth`, usuario);

  }

  createOrUpdate(usuario : any){
    console.log("Metodo createOrUpdate_UsuarioService");    
    if(usuario._id !=null && usuario._id != ''){
      console.log("Metodo createOrUpdate_UsuarioService_Actualizar");    
      console.log(usuario);
      const body = {
                    _id : usuario._id, 
                    cui : usuario.cui,
                    nombre : usuario.nombre,
                    email : usuario.email,
                    password : usuario.password,
                    perfil : usuario.perfil
                  };
      return this.http.put<any>(`${MESA_AYUDA_API}/${usuario._id }`,body);
    }else{
      console.log("Metodo createOrUpdate_UsuarioService_Crear");    
      console.log(usuario);
      usuario._id = null;
      return this.http.post(`${MESA_AYUDA_API}`,new UsuarioBD(usuario.cui,usuario.nombre,usuario.email,usuario.password,usuario.perfil));
    }
  }

  findAll(){
    return this.http.get(`${MESA_AYUDA_API}`);
  }

  findById(id: string){
    return this.http.get(`${MESA_AYUDA_API}/${id}`);
  }

  delete(id: string){
    return this.http.delete(`${MESA_AYUDA_API}/${id}`);
  }
}
