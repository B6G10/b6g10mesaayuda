import { Injectable, EventEmitter } from '@angular/core';
import { Usuario } from '../@modelos/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public static instance : SharedService;
  usuario: Usuario | any;
  token: string | undefined | null;
  showTemplate = new EventEmitter<boolean>();


  constructor() {
    this.usuario = new Usuario('','','','','','');
    return SharedService.instance = SharedService.instance || this;
   }

   public static getInstance(){
    if(this.instance == null){
      this.instance = new SharedService();
    }
    return this.instance;
  }

  isLoggedIn():boolean {
    if(this.usuario == null){
      return false;
    }
    return this.usuario.email != '';
  }
}
