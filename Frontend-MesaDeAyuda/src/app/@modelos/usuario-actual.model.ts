import { Usuario } from './usuario.model';

export class CurrentUser{
    public token: string | undefined;
    public usuario: Usuario | undefined;
}