import { Usuario } from './usuario.model';
export class TicketBD {

    constructor(
    public _id: string | null,
    public titulo: string | null,
    public descripcion: string,
    public estado: string,
    public usuarioCrea: string | null,
    public usuarioAsignado: string | null,
    public fechaCierre: string | null,
    public changes: Array<string> | null,
    public createdAt : string | null,
    public updatedAt : string | null
    ){}
}
