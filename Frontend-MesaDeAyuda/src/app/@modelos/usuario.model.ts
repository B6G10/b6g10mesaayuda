export class Usuario {
    constructor(
        public _id : string | null,
        public cui: string | null,
        public nombre : string,
        public email: string,
        public password: string,
        public perfil: string
    ){}
}
