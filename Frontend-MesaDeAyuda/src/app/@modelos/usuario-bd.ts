export class UsuarioBD {
    constructor(
        public cui: string | null,
        public nombre : string,
        public email: string,
        public password: string,
        public perfil: string
    ){}
}
